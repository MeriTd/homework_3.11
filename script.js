/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */







/*
Теоретичні питання
1.
Подія – це сигнал від браузера у тому, що щось сталося. 
Використовуєтсья для реакції на дії користувача.


2. 
mousedown/mouseup - Кнопка миші натиснута/відпущена над елементом.
mouseover/mouseout - Курсор миші з'являється над елементом і йде з нього.
mousemove - Кожен рух миші над елементом генерує цю подію.
click - Викликається одинарним кліком лівої кнопки миші
dblclick - Викликається подвійним кліком на елементі


3.
contextmenu – викликається для відкриття контекстного меню натисканням правої кнопки миші. 
Може викликатись правою кнопкою миші, а може викликатись кнопкою на клавіатурі. 
Має три варіанта використання:
.contextmenu() - просто виклик контесктного меню
.contextmenu(handler) - де, handler - функція яка буде встановлена в якості обробника
.contextmenu([eventData], handler) - де eventData - об'якт з данними для передачі в обробник.
*/










/*
Практичні завдання
1. Додати новий абзац по кліку на кнопку:
По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
*/


const btn = document.querySelector('#btn-click');

btn.addEventListener('click', () => {
   btn.insertAdjacentHTML('afterend', '<p>New Paragraph</p>');
});

document.querySelector('#content').append(btn);






/*
2. Додати новий елемент форми із атрибутами:
Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, 
наприклад, type, placeholder, і name. та додайте його під кнопкою.
*/



const btnCreate = document.createElement('button');
btnCreate.innerText = 'btn Create';
btnCreate.id = 'btn-input-create';

document.querySelector('#content').append(btnCreate);


btnCreate.addEventListener('click', () => {
   const createBtn = document.createElement('input');

   createBtn.type = 'name';
   createBtn.placeholder = 'name';
   createBtn.name = 'name';

   btnCreate.after(createBtn);
});